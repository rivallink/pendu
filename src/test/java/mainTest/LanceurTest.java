package mainTest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import main.Lanceur;
import modele.PenduClassique;
import modele.PenduTordu;

public class LanceurTest {

    @Test
    public void testLanceur() {
        String[] args = new String[3];
        args[0] = "0";
        args[1] = "0";
        args[2] = "0";
        Lanceur lanceur = new Lanceur(args);
        assertEquals(lanceur.getArgs().length, 3);
    }

    @Test
    public void testAnalyseArgsClassiqueNiv0Fixe() {
        String[] args = new String[3];
        args[0] = "0";
        args[1] = "0";
        args[2] = "0";
        Lanceur lanceur = new Lanceur(args);
        lanceur.analyseArgs();

        assertEquals(lanceur.getMoteur().getClass(), PenduClassique.class);
        assertEquals(lanceur.getMoteur().getNiveau(), "0");
        assertEquals(lanceur.getMoteur().getTypePendu(), "fixe");
    }

    @Test
    public void testAnalyseArgsClassiqueNiv1Fichier() {
        String[] args = new String[3];
        args[0] = "1";
        args[1] = "1";
        args[2] = "1";
        Lanceur lanceur = new Lanceur(args);
        lanceur.analyseArgs();

        assertEquals(lanceur.getMoteur().getClass(), PenduTordu.class);
        assertEquals(lanceur.getMoteur().getNiveau(), "1");
        assertEquals(lanceur.getMoteur().getTypePendu(), "fichier");
    }

    @Test
    public void testGetArgs() {
        String[] args = new String[3];
        args[0] = "0";
        args[1] = "0";
        args[2] = "0";
        Lanceur lanceur = new Lanceur(args);
        assertEquals(lanceur.getArgs().length, 3);
    }

}
