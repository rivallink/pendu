package vueTest;

import static org.junit.Assert.*;

import org.junit.Test;

import vue.PenduConsole;

public class PenduConsoleTest {
	
	private static final String RIEN = "                \n";
	private static final String POTEAU = "|              \n";
	private static final String BASE = "____________  \n";
	private static final String SUPPORT = " _________     \n";
	private static final String OBLIQUE = "|/             \n";
	private static final String CORDE = "|/        |    \n";
	private static final String TETE = "|         0    \n";
	private static final String TRONC = "|         |    \n";
	private static final String BRASGAUCHE = "|        /|    \n";
	private static final String BRASDROITE = "|        /|\\  \n";
	private static final String JAMBEGAUCHE= "|        /     \n";
	private static final String JAMBEDROITE = "|        / \\  \n";


	@Test
	public void testPotenceVide() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(0);
		assertEquals(potence.length(), 0);	
	}
	
	@Test
	public void testPotence1() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(1);
        StringBuilder resultatAttendu = new StringBuilder(120);
        resultatAttendu.append(BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());
	}
	
	@Test
	public void testPotence2() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(2);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(RIEN + POTEAU + POTEAU + POTEAU + POTEAU + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());
	}
	
	@Test
	public void testPotence3() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(3);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + POTEAU + POTEAU + POTEAU + POTEAU + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}
	
	@Test
	public void testPotence4() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(4);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + OBLIQUE + POTEAU + POTEAU + POTEAU + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}
	
	@Test
	public void testPotence5() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(5);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + POTEAU + POTEAU + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}
	
	@Test
	public void testPotence6() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(6);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + TRONC + POTEAU + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}	
	
	@Test
	public void testPotence7() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(7);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + BRASGAUCHE + POTEAU + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}	
	
	@Test
	public void testPotence8() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(8);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + BRASDROITE + POTEAU + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}
	
	@Test
	public void testPotence9() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(9);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + BRASDROITE + JAMBEGAUCHE + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}
	
	@Test
	public void testPotence10() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(10);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}
	
	@Test
	public void testPotence11() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(11);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}
	
	@Test
	public void testPotence12() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(12);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE + RIEN + POTEAU + POTEAU + POTEAU + POTEAU + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}
	
	@Test
	public void testPotence13() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(13);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE + SUPPORT + POTEAU + POTEAU + POTEAU + POTEAU + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}	
	
	@Test
	public void testPotence14() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(14);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE + SUPPORT + OBLIQUE + POTEAU + POTEAU + POTEAU + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}
	
	@Test
	public void testPotence15() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(15);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE + SUPPORT + CORDE + TETE + POTEAU + POTEAU + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}
	
	@Test
	public void testPotence16() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(16);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE + SUPPORT + CORDE + TETE + TRONC + POTEAU + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}
	
	@Test
	public void testPotence17() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(17);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE + SUPPORT + CORDE + TETE + BRASGAUCHE + POTEAU + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}
	
	@Test
	public void testPotence18() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(18);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE + SUPPORT + CORDE + TETE + BRASDROITE + POTEAU + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}
	
	@Test
	public void testPotence19() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(19);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE + SUPPORT + CORDE + TETE + BRASDROITE + JAMBEGAUCHE + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}
	
	@Test
	public void testPotence20() {
		PenduConsole pc = new PenduConsole();
		StringBuilder potence = pc.affichagePotence(20);
		StringBuilder resultatAttendu = new StringBuilder(120);
		resultatAttendu.append(SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE
                + SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE);
		assertEquals(potence.toString(), resultatAttendu.toString());	
	}

}
