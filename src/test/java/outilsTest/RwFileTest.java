package outilsTest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import outils.RwFileOutil;

public class RwFileTest {

    @Test
    public void testReadFile() {
        List<String> resultatAttendu = RwFileOutil.readFile("data\\words.txt");
        assertEquals(resultatAttendu.size() > 0, true);
    }

    @Test
    public void testReadNotFile() {
        List<String> resultatAttendu = RwFileOutil.readFile("pas un fichier");
        assertEquals(resultatAttendu.size() > 0, false);
    }

    @Test
    public void testWriteFile() {
        List<String> nouveauMot = new ArrayList<String>();
        nouveauMot.add("killian");
        RwFileOutil.writeFile(nouveauMot, "data\\killian.txt");
        List<String> listeMot = RwFileOutil.readFile("data\\killian.txt");
        assertEquals("KILLIAN", listeMot.get(0));
    }
}
