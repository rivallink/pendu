package outilsTest;

import static org.junit.Assert.*;

import org.junit.Test;

import outils.EstChiffreOutil;

public class EstChiffreTest {

    @Test
    public void testEstChiffre() {
        assertEquals(EstChiffreOutil.main("0"), true);
    }

    @Test
    public void testEstPasChiffre() {
        assertEquals(EstChiffreOutil.main("paschiffre"),false);
    }

}
