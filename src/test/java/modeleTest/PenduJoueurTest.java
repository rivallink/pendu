package modeleTest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import modele.PenduFactory;
import modele.PenduJoueur;

public class PenduJoueurTest {

    @Test
    public void testPenduJoueur() {
        PenduJoueur jo = new PenduJoueur("1", new PenduFactory().create("classique", "0", "1"),0);
        assertEquals(jo.getIdentifiant(), "1");
        assertNotEquals(jo.getPendu(), null);
    }

    @Test
    @Ignore
    public void testJouer() {
        PenduJoueur jo = new PenduJoueur("1", new PenduFactory().create("classique", "0", "1"),0);
        assertEquals(jo.jouer(), false);
    }
}
