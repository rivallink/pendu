package modeleTest;

import static org.junit.Assert.*;

import org.junit.Test;

import modele.PenduClassique;
import modele.PenduTordu;

public class PenduClassiqueTest {

   @Test
    public void testNombreEssai() {
        PenduClassique pt = new PenduClassique("0","0");
        assertEquals(pt.getNombreEssai(), 10);
    }

    @Test
    public void testEssayerLettreNok() {
        PenduClassique pt = new PenduClassique("0","0");
        pt.setMotATrouver("BIERE");
        pt.setMotActuel();
        int dec = pt.getDecouvert();
        assertEquals(pt.essayerLettre('L'), false);
        assertEquals(pt.getMotActuel().toString(), "-----");
        assertEquals(pt.getDecouvert(), dec);
    }

    @Test
    public void testEssayerLettreOk() {
        PenduClassique pt = new PenduClassique("0","0");
        pt.setMotATrouver("BIERE");
        pt.setMotActuel();
        int dec = pt.getDecouvert();
        assertEquals(pt.essayerLettre('E'), true);
        assertEquals(pt.getMotActuel().toString(), "--E-E");
        assertEquals(pt.getDecouvert(), dec+2);
    }

    @Test
    public void testEssayerLettreDejaTrouveeOk() {
        PenduClassique pt = new PenduClassique("0","0");
        pt.setMotATrouver("BIERE");
        pt.setMotActuel();
        pt.essayerLettre('E');
        int dec = pt.getDecouvert();
        assertEquals(pt.essayerLettre('E'), true);
        assertEquals(pt.getMotActuel().toString(), "--E-E");
        assertEquals(pt.getDecouvert(), dec+2);
    }

    @Test
    public void testGetNombreEssai() {
        PenduClassique pt = new PenduClassique("0","0");
        assertEquals(pt.getNombreEssai(), 10);
    }
}
