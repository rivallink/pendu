package modeleTest;

import static org.junit.Assert.*;

import org.junit.Test;

import modele.PenduClassique;
import modele.PenduTordu;

public class PenduTorduTest {

    @Test
    public void testNombreEssai() {
        PenduTordu pt = new PenduTordu("0","0");
        assertEquals(pt.getNombreEssai(), 20);
    }

    @Test
    public void testEssayerLettreNok() {
        PenduTordu pt = new PenduTordu("0","0");
        pt.setMotATrouver("BIERE");
        pt.setMotActuel();
        assertEquals(pt.essayerLettre('L'), false);
    }

    @Test
    public void testEssayerLettreOk() {
        PenduTordu pt = new PenduTordu("0","0");
        pt.setMotATrouver("BIERE");
        pt.setMotActuel();
        assertEquals(pt.essayerLettre('e'), true);
    }

    @Test
    public void testEssayerLettreDejaTrouveeOk() {
        PenduTordu pt = new PenduTordu("0","0");
        pt.setMotATrouver("BIERE");
        pt.setMotActuel();
        pt.essayerLettre('i');
        pt.essayerLettre('e');
        assertEquals(pt.essayerLettre('e'), true);
    }

    @Test
    public void testEssayerLettreDejaTrouveeNok() {
        PenduTordu pt = new PenduTordu("0","0");
        pt.setMotATrouver("BIERE");
        pt.setMotActuel();
        pt.essayerLettre('i');
        pt.essayerLettre('e');
        pt.essayerLettre('R');
        assertEquals(pt.essayerLettre('R'), false);
    }

    @Test
    public void testGetNombreEssai() {
        PenduTordu pt = new PenduTordu("0","0");
        assertEquals(pt.getNombreEssai(), 20);
    }
}
