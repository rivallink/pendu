package modeleTest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import modele.Multi;

public class MultiTest {

    @Test
    public void testMulti() {
        Multi multi = new Multi(2,1,"classique", "0","fixe");
        assertNotEquals(multi.getListeParticipants(), null);
        assertEquals(multi.getListeParticipants().size(),3);
    }

    @Test
    @Ignore
    public void testPartie() {
        Multi multi = new Multi(0,0,"classique", "0","fixe");
        multi.partie();
        assertEquals(multi.getListeParticipants().size(),2);
        assertEquals(multi.getListeParticipants().size(),2);
    }

    @Test
    @Ignore
    public void testPartieIA() {
        Multi multi = new Multi(2,0,"classique", "0","fixe");
        multi.partie();
        assertEquals(multi.getListeParticipants().size(),0);
        assertEquals(multi.getListeParticipants().size(),0);
    }

    @Test
    @Ignore
    public void testPartieJoueur() {
        Multi multi = new Multi(0,2,"classique", "0","fixe");
        multi.partie();
        assertEquals(multi.getListeParticipants().size(),0);
        assertEquals(multi.getListeParticipants().size(),0);
    }

    @Test
    @Ignore
    public void testPartieJoueurIA() {
        Multi multi = new Multi(1,2,"classique", "0","fixe");
        multi.partie();
        assertEquals(multi.getListeParticipants().size(),0);
        assertEquals(multi.getListeParticipants().size(),0);
    }
}
