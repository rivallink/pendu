package modeleTest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import modele.ListeMotsFixe;

public class ListeMotsFixeTest {

    @Test
    public void testGetMots() {
        ListeMotsFixe lmf = new ListeMotsFixe();
        assertNotEquals(lmf.getMots().size(), 0);
    }

    @Test
    public void testSetMots() {
        ListeMotsFixe lmf = new ListeMotsFixe();
        ArrayList<String> mots = new ArrayList<String>();
        mots.add("BUT");
        mots.add("NUIT");
        mots.add("RIZ");
        mots.add("BIERE");
        lmf.setMots(mots);
        assertEquals(lmf.getMots().size(), 4);
    }
}
