package modeleTest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import modele.PenduFactory;
import modele.PenduTordu;
import modele.PenduClassique;

public class PenduFactoryTest {


    @Test
    public void testChargeListeMotsFactoryFichier() {
        PenduFactory pf = new PenduFactory();
        assertEquals(pf.create("classique", "0", "0").getClass(), PenduClassique.class);
    }

    @Test
    public void testChargeListeMotsFactoryFixe() {
        PenduFactory pf = new PenduFactory();
        assertEquals(pf.create("tordu", "0", "0").getClass(), PenduTordu.class);
    }

    @Test
    public void testChargeListeMotsFactoryNull() {
        PenduFactory pf = new PenduFactory();
        assertEquals(pf.create("noType", "0", "0"), null);
    }

    @Test
    public void testChargeListeMotsThreeParams() {
        PenduFactory pf = new PenduFactory();
        assertEquals(pf.create("classique", ""), null);
    }
}
