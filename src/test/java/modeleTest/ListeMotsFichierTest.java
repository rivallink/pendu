package modeleTest;

import static org.junit.Assert.*;
import org.junit.Ignore;

import java.util.ArrayList;

import org.junit.Test;

import modele.ListeMotsFichier;

public class ListeMotsFichierTest {

    @Test
    public void testChargeListeMotsFichierOk() {
        ListeMotsFichier lmf = new ListeMotsFichier("data\\killian.txt");
        assertEquals(lmf.getMots().size(), 1);
    }

    @Test
    public void testChargeListeMotsFichierNOk() {
        ListeMotsFichier lmf = new ListeMotsFichier("data\\noFile.txt");
        assertEquals(lmf.getMots(), null);
    }

    @Test
    public void testSetMots() {
        ListeMotsFichier lmf = new ListeMotsFichier("data\\killian.txt");
        ArrayList<String> mots = new ArrayList<String>();
        mots.add("BUT");
        mots.add("NUIT");
        mots.add("RIZ");
        mots.add("BIERE");
        lmf.setMots(mots);
        assertEquals(lmf.getMots().size(), 4);
    }
}
