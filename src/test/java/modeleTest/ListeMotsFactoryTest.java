package modeleTest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import modele.ListeMotsFactory;
import modele.ListeMotsFichier;
import modele.ListeMotsFixe;

public class ListeMotsFactoryTest {


    @Test
    public void testChargeListeMotsFactoryFichier() {
        ListeMotsFactory lmf = new ListeMotsFactory();
        assertEquals(lmf.create("fichier", "data\\killian.txt").getClass(), ListeMotsFichier.class);
    }

    @Test
    public void testChargeListeMotsFactoryFixe() {
        ListeMotsFactory lmf = new ListeMotsFactory();
        assertEquals(lmf.create("fixe", "").getClass(), ListeMotsFixe.class);
    }

    @Test
    public void testChargeListeMotsThreeParams() {
        ListeMotsFactory lmf = new ListeMotsFactory();
        assertEquals(lmf.create("fixe", "",""), null);
    }
}
