package modeleTest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import modele.PenduFactory;
import modele.PenduIA;

public class PenduIATest {

    @Test
    public void testPenduIA() {
        PenduIA ia = new PenduIA("1", new PenduFactory().create("classique", "0", "1"),0);
        assertEquals(ia.getIdentifiant(), "1");
        assertNotEquals(ia.getPendu(), null);
    }

    @Test
    @Ignore
    public void testJouer() {
        PenduIA ia = new PenduIA("1", new PenduFactory().create("classique", "0", "1"),0);
        assertEquals(ia.jouer(), false);
    }
}
