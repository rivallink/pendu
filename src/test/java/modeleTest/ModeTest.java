package modeleTest;

import static org.junit.Assert.*;

import org.junit.Ignore;

import modele.PenduClassique;
import modele.PenduFactory;
import modele.PenduModel;

import org.junit.Test;

/**
 * Classe de test pour le Mode.
 * @author Rivallin
 *
 */
public class ModeTest {

    /*
     * Permet de tester que le joueur � choisi le mode debutant.
     */

    @Test
    @Ignore
    public void testDebutant() {
        // Le joueur est dans le mode debutant si le mot BUT dans la liste des mots du moteur
        //PenduFactory testPenduClassique = new PenduFactory();
        //testPenduClassique.create("classique", "0", "0");
        //assertEquals(testPenduClassique.getMots().contains("BUT"), true);
    }

    @Test
    @Ignore
    public void testExpert() {
        // Le joueur est dans le mode expert si le mot BIERE dans la liste des mots du moteur
    }

}
