package modeleTest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import modele.PenduClassique;
import modele.PenduModel;

public class PenduModelTest {

    @Test
    public void testInit() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setChemin(null);
        pendu.setTypePendu("fixe");
        pendu.init();

        ArrayList<String> mots = new ArrayList<String>();
        mots.add("BUT");
        mots.add("NUIT");
        mots.add("RIZ");
        mots.add("BIERE");
        pendu.setMots(mots);

        assertEquals(pendu.isVictoire(), false);
        assertEquals(pendu.getDecouvert(), 0);
        assertEquals(pendu.getErreurs(), 0);
        assertNotEquals(pendu.getMotATrouver(), null);
    }

    @Test
    public void testCreateList() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setChemin(null);
        pendu.setTypePendu("fixe");
        pendu.createList();
        assertNotEquals(pendu.getBaseMots().getMots().size(), 0);
        assertNotEquals(pendu.getMots().size(), 0);
    }

    @Test
    public void testChoixMotsNull() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setMots(null);

        pendu.choixMot();

        assertEquals(pendu.getMotATrouver(), null);
    }

    @Test
    public void testChoixMotsNotNullNiv0() {
        PenduModel pendu = new PenduClassique("0", "0");

        ArrayList<String> mots = new ArrayList<String>();
        mots.add("BUT");
        mots.add("NUIT");
        mots.add("RIZ");
        mots.add("BIERE");
        pendu.setMots(mots);

        pendu.choixMot();

        assertNotEquals(pendu.getMotATrouver(), null);
        assertEquals(pendu.getMotActuel().length(), pendu.getMotATrouver().length());
    }

    @Test
    @Ignore
    public void testChoixMotsNotNullNiv0MotInf3() {
        PenduModel pendu = new PenduClassique("0", "0");

        ArrayList<String> mots = new ArrayList<String>();
        mots.add("LE");
        pendu.setMots(mots);

        pendu.choixMot();

        assertNotEquals(pendu.getMotATrouver(), null);
        assertEquals(pendu.getMotActuel().length(), pendu.getMotATrouver().length());
    }

    @Test
    @Ignore
    public void testChoixMotsNotNullNiv0MotSup4() {
        PenduModel pendu = new PenduClassique("0", "0");

        ArrayList<String> mots = new ArrayList<String>();
        mots.add("BIERE");
        pendu.setMots(mots);

        pendu.choixMot();

        assertNotEquals(pendu.getMotATrouver(), null);
        assertEquals(pendu.getMotActuel().length(), pendu.getMotATrouver().length());
    }

    @Test
    public void testChoixMotsNotNullNiv0MotEntre3Et4() {
        PenduModel pendu = new PenduClassique("0", "0");

        ArrayList<String> mots = new ArrayList<String>();
        mots.add("BON");
        pendu.setMots(mots);

        pendu.choixMot();

        assertNotEquals(pendu.getMotATrouver(), null);
        assertEquals(pendu.getMotActuel().length(), pendu.getMotATrouver().length());
    }

    @Test
    public void testChoixMotsNotNullNiv1() {
        PenduModel pendu = new PenduClassique("1", "0");

        ArrayList<String> mots = new ArrayList<String>();
        mots.add("BUT");
        mots.add("NUIT");
        mots.add("RIZ");
        mots.add("BIERE");
        pendu.setMots(mots);

        pendu.choixMot();

        assertNotEquals(pendu.getMotATrouver(), null);
        assertEquals(pendu.getMotActuel().length(), pendu.getMotATrouver().length());
    }

    @Test
    public void testChoixMotsNotNullNivX() {
        PenduModel pendu = new PenduClassique("2", "0");

        ArrayList<String> mots = new ArrayList<String>();
        mots.add("BUT");
        mots.add("NUIT");
        mots.add("RIZ");
        mots.add("BIERE");
        pendu.setMots(mots);

        pendu.choixMot();

        assertNotEquals(pendu.getMotATrouver(), null);
        assertEquals(pendu.getMotActuel().length(), pendu.getMotATrouver().length());
    }

    @Test
    public void testTourErreurNonPerdu() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setMotATrouver("BIERE");
        pendu.setMotActuel();
        pendu.setNombreEssai(10);

        pendu.setErreurs(2);

        assertEquals(pendu.tour('L'), false);
        assertEquals(pendu.getErreurs(), 3);
    }

    @Test
    public void testTourErreurPerdu() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setMotATrouver("BIERE");
        pendu.setMotActuel();
        pendu.setNombreEssai(10);

        pendu.setErreurs(9);

        assertEquals(pendu.tour('L'), true);
        assertEquals(pendu.getErreurs(), 10);
    }

    @Test
    public void testTourBonneLettreNonVictoire() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setMotATrouver("BIERE");
        pendu.setMotActuel();
        pendu.setNombreEssai(10);
        pendu.essayerLettre('E');
        pendu.essayerLettre('R');

        pendu.setErreurs(5);

        assertEquals(pendu.tour('I'), false);
        assertEquals(pendu.getErreurs(), 5);
        assertEquals(pendu.isVictoire(), false);
    }

    @Test
    public void testTourBonneLettreVictoire() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setMotATrouver("BIERE");
        pendu.setMotActuel();
        pendu.setNombreEssai(10);
        pendu.setDecouvert(4);
        pendu.setErreurs(5);
        pendu.essayerLettre('B');
        pendu.essayerLettre('E');
        pendu.essayerLettre('R');

        assertEquals(pendu.tour('I'), true);
        assertEquals(pendu.getErreurs(), 5);
        assertEquals(pendu.isVictoire(), true);
    }

    @Test
    public void testGetNiveau() {
        PenduModel pendu = new PenduClassique("0", "0");
        assertEquals(pendu.getNiveau(), "0");
    }

    @Test
    public void testSetNiveau() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setNiveau("1");
        assertEquals(pendu.getNiveau(), "1");
    }

    @Test
    public void testGetMots() {
        PenduModel pendu = new PenduClassique("0", "0");
        assertEquals(pendu.getBaseMots(), null);
    }

    @Test
    public void testSetMots() {
        PenduModel pendu = new PenduClassique("0", "0");

        ArrayList<String> mots = new ArrayList<String>();
        mots.add("BUT");
        mots.add("NUIT");
        mots.add("RIZ");
        mots.add("BIERE");
        pendu.setMots(mots);

        assertEquals(pendu.getMots().size(), 4);
    }

    @Test
    public void testGetTypePendu() {
        PenduModel pendu = new PenduClassique("0", "0");
        assertEquals(pendu.getTypePendu(), "0");
    }

    @Test
    public void testSetTypePendu() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setTypePendu("1");
        assertEquals(pendu.getTypePendu(), "1");
    }

    @Test
    public void testGetChemin() {
        PenduModel pendu = new PenduClassique("0", "0");
        assertEquals(pendu.getChemin(), null);
    }

    @Test
    public void testSetChemin() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setChemin("data\\killian.txt");
        assertEquals(pendu.getChemin(), "data\\killian.txt");
    }

    @Test
    public void testGetMotATrouver() {
        PenduModel pendu = new PenduClassique("0", "0");
        assertEquals(pendu.getMotATrouver(), null);
    }

    @Test
    public void testSetMotATrouver() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setMotATrouver("BIERE");
        assertEquals(pendu.getMotATrouver(), "BIERE");
    }

    @Test
    public void testGetMotActuel() {
        PenduModel pendu = new PenduClassique("0", "0");
        assertNotEquals(pendu.getMotActuel(), null);
    }

    @Test
    public void testSetMotActuel() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setMotATrouver("BIERE");
        pendu.setMotActuel();
        pendu.essayerLettre('B');
        pendu.essayerLettre('R');
        assertEquals(pendu.getMotActuel().toString(), "B--R-");
    }

    @Test
    public void testIsVictoire() {
        PenduModel pendu = new PenduClassique("0", "0");
        assertEquals(pendu.isVictoire(), false);
    }

    @Test
    public void testSetVictoire() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setVictoire(true);
        assertEquals(pendu.isVictoire(), true);
    }

    @Test
    public void testGetDecouvert() {
        PenduModel pendu = new PenduClassique("0", "0");
        assertEquals(pendu.getDecouvert(), 0);
    }

    @Test
    public void testSetDecouvert() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setDecouvert(3);
        assertEquals(pendu.getDecouvert(), 3);
    }

    @Test
    public void testGetErreurs() {
        PenduModel pendu = new PenduClassique("0", "0");
        assertEquals(pendu.getErreurs(), 0);
    }

    @Test
    public void testSetErreurs() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setErreurs(3);
        assertEquals(pendu.getErreurs(), 3);
    }

    @Test
    public void testGetNombreEssai() {
        PenduModel pendu = new PenduClassique("0", "0");
        assertEquals(pendu.getNombreEssai(), 10);
    }

    @Test
    public void testSetNombreEssai() {
        PenduModel pendu = new PenduClassique("0", "0");
        pendu.setNombreEssai(20);
        assertNotEquals(pendu.getNombreEssai(), 20);
    }
}
