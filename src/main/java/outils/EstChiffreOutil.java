package outils;

/**
 * Classe pour verifier qu'une chaine est un chiffre.
 * @author Rivallin
 *
 */
public final class EstChiffreOutil {

    private EstChiffreOutil() {

    }

    /**
     * Verifier si le champ String passe en parametre est un chiffre.
     * @param chaineATester la chaine a tester
     * @return True s'il correspond a un Chiffre / False sinon
     */
    public static boolean main(final String chaineATester) {
        try {
            Double.parseDouble(chaineATester);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
