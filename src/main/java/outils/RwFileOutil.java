package outils;

import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

/**
 * Cette classe contient des methodes utilitaires pour traiter les fichiers.
 */
@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
public final class RwFileOutil {

    private final static String FICHIERNONTROUVE = "Fichier non trouve : '";
    private final static String COTE = "'";

    private RwFileOutil() {

    }

    /**
     * Cette methode est utilise pour lire un fichier de texte ligne par ligne et restitue le contenu dans un List.
     *
     * @param fileName le fichier a lister
     * @return une List contenant les lignes du fichier
     */
    public static List<String> readFile(final String fileName) {
        final List<String> file = new ArrayList<String>();

        try {
            // ouverture du fichier
            final Scanner filein = new Scanner(new FileReader(fileName));
            filein.useLocale(Locale.getDefault());

            // lecture et ajout des lignes une par une
            while (filein.hasNextLine()) {
                String ligne = filein.nextLine().toUpperCase(Locale.getDefault());
                if (ligne.matches("[A-Z]+")) {
                    file.add(ligne);
                }
            }

            // fermeture du fichier ouvert en lecture
            filein.close();
        } catch (FileNotFoundException e) {
            System.out.println(FICHIERNONTROUVE + fileName + COTE);
        }
        return file;
    } // -------------------------------------------------- readFile()

    /**
     * Cette methode est utile pour ecrire un List dans un fichier de texte un element par ligne.
     * @param liste    un List contenant les donnees
     * @param fileName le nom du fichier a ecrire
     */
    public static void writeFile(final List<String> liste, final String fileName) {
        try {
            // ouverture du fichier a ecrire
            final PrintWriter out = new PrintWriter(fileName);
            for (final String ligne : liste) {
                out.println(ligne);
            }
            // fermeture du fichier
            out.close();

        } catch (FileNotFoundException e) {
            System.out.println(FICHIERNONTROUVE + fileName + COTE);

        }
    } // -------------------------------------------------- writeFile()

    /**
     * Cette methode sert a ecrire un HashMap dans un fichier de texte : la cle et la valeur sont ecrits sur une ligne.
     * @param map      un HasMap contenant les donnees
     * @param fileName le nom du fichier a ecrire
     */
    public static void writeMap(final Map<String, Double> map, final String fileName) {

        try {
            // ouverture du fichier a ecrire
            final PrintWriter out = new PrintWriter(fileName);
            for (final String key : map.keySet()) {
                out.println(key + "     " + map.get(key).toString()); // concatene une chaine et un Double.toString()
            }
            // fermeture du fichier
            out.close();

        } catch (FileNotFoundException e) {
            System.out.println(FICHIERNONTROUVE + fileName + COTE);
        }
    } // -------------------------------------------------- writeMap()

}
