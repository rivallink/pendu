package vue;

/**
 * Representation du pendu dans la console.
 * @author Rivallin
 *
 */
public class PenduConsole {

    private static final String RIEN = "                \n";
    private static final String POTEAU = "|              \n";
    private static final String BASE = "____________  \n";
    private static final String SUPPORT = " _________     \n";
    private static final String OBLIQUE = "|/             \n";
    private static final String CORDE = "|/        |    \n";
    private static final String TETE = "|         0    \n";
    private static final String TRONC = "|         |    \n";
    private static final String BRASGAUCHE = "|        /|    \n";
    private static final String BRASDROITE = "|        /|\\  \n";
    private static final String JAMBEGAUCHE = "|        /     \n";
    private static final String JAMBEDROITE = "|        / \\  \n";
    private static final String PREMIERPENDU = SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE;

    /**
     * Afficher la potence dans la console.
     * @param statut la nombre d'erreur actuel
     * @return Une chaine en ASCII representant la potence
     */
    public StringBuilder affichagePotence(final int statut) {

        final StringBuilder resultat = new StringBuilder(120);

        switch (statut) {

        case 0:
            break;

        case 1:
            resultat.append(BASE);
            break;

        case 2:
            resultat.append(RIEN + POTEAU + POTEAU + POTEAU + POTEAU + POTEAU + BASE);
            break;

        case 3:
            resultat.append(SUPPORT + POTEAU + POTEAU + POTEAU + POTEAU + POTEAU + BASE);
            break;

        case 4:
            resultat.append(SUPPORT + OBLIQUE + POTEAU + POTEAU + POTEAU + POTEAU + BASE);
                break;

        case 5:
            resultat.append(SUPPORT + CORDE + TETE + POTEAU + POTEAU + POTEAU + BASE);
            break;

        case 6:
            resultat.append(SUPPORT + CORDE + TETE + TRONC + POTEAU + POTEAU + BASE);
            break;

        case 7:
            resultat.append(SUPPORT + CORDE + TETE + BRASGAUCHE + POTEAU + POTEAU + BASE);
            break;

        case 8:
            resultat.append(SUPPORT + CORDE + TETE + BRASDROITE + POTEAU + POTEAU + BASE);
            break;

        case 9:
            resultat.append(SUPPORT + CORDE + TETE + BRASDROITE + JAMBEGAUCHE + POTEAU + BASE);
            break;


        case 10:
            resultat.append(SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE);
            break;

        case 11:
            resultat.append(PREMIERPENDU + BASE);
            break;

        case 12:
            resultat.append(PREMIERPENDU + RIEN + POTEAU + POTEAU + POTEAU + POTEAU + POTEAU + BASE);
            break;

        case 13:
            resultat.append(PREMIERPENDU + SUPPORT + POTEAU + POTEAU + POTEAU + POTEAU + POTEAU + BASE);
            break;

        case 14:
            resultat.append(PREMIERPENDU + SUPPORT + OBLIQUE + POTEAU + POTEAU + POTEAU + POTEAU + BASE);
                break;

        case 15:
            resultat.append(PREMIERPENDU + SUPPORT + CORDE + TETE + POTEAU + POTEAU + POTEAU + BASE);
            break;

        case 16:
            resultat.append(PREMIERPENDU + SUPPORT + CORDE + TETE + TRONC + POTEAU + POTEAU + BASE);
            break;

        case 17:
            resultat.append(PREMIERPENDU + SUPPORT + CORDE + TETE + BRASGAUCHE + POTEAU + POTEAU + BASE);
            break;

        case 18:
            resultat.append(PREMIERPENDU + SUPPORT + CORDE + TETE + BRASDROITE + POTEAU + POTEAU + BASE);
            break;

        case 19:
            resultat.append(PREMIERPENDU + SUPPORT + CORDE + TETE + BRASDROITE + JAMBEGAUCHE + POTEAU + BASE);
            break;

        case 20:
            resultat.append(PREMIERPENDU + SUPPORT + CORDE + TETE + BRASDROITE + JAMBEDROITE + POTEAU + BASE);
            break;

        default:
            break;
        }

        return resultat;
    }
}
