package modele;

import java.util.Scanner;

import vue.PenduConsole;

/**
 * Classe qui simule le comportement d'un joueur ordinateur.
 *
 * @author Rivallin
 *
 */
@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
public class PenduJoueur implements PenduParticipant {

    /**
     * Identifiant du joueur.
     */
    private final String identifiantJoueur;

    /**
     * Pendu du joueur.
     */
    private final PenduModel penduJoueur;

    /**
     * Nombre de mots trouvees par le joueur.
     */
    private int nbMotsTrouveesJoueur;

   /**
    * Constructeur de la classe.
    * @param identifiantJoueur - identifiant du joueur.
    * @param penduJoueur - pendu du joueur.
    * @param nbMotsTrouveesJoueur - nombre de mots trouvees du joueur.
    */
    public PenduJoueur(final String identifiantJoueur, final PenduModel penduJoueur, final int nbMotsTrouveesJoueur) {
        this.nbMotsTrouveesJoueur = nbMotsTrouveesJoueur;
        this.identifiantJoueur = identifiantJoueur;
        this.penduJoueur = penduJoueur;
    }

    /**
     * Simule le comportement de l'IA.
     * @return true si il peut continuer a jouer
     */
    public boolean jouer() {
        final PenduConsole afficheur = new PenduConsole();
        System.out.println();
        System.out.println(this.penduJoueur.getMotActuel());
        System.out.println("Nombre d'essais :" + (this.penduJoueur.getNombreEssai() - this.penduJoueur.getErreurs()));
        Scanner clavierJouer = new Scanner(System.in);
        String scanJouer = clavierJouer.nextLine();
        while (scanJouer.length() < 1) {
            scanJouer = clavierJouer.nextLine();
        }
        while (!this.penduJoueur.tour(scanJouer.charAt(0))) {
            System.out.println(afficheur.affichagePotence(this.penduJoueur.getErreurs()));
            System.out.println(this.penduJoueur.getMotActuel());
            System.out.println("Essais restant :" + (this.penduJoueur.getNombreEssai() - this.penduJoueur.getErreurs()));
            scanJouer = clavierJouer.nextLine();
            while (scanJouer.length() < 1) {
                scanJouer = clavierJouer.nextLine();
            }
        }
        if (this.penduJoueur.isVictoire()) {
            System.out.println("GAGNE ! \n Vous avez trouve le mot :" + this.penduJoueur.getMotATrouver()
                    + "\n essais restant :" + (this.penduJoueur.getNombreEssai() - this.penduJoueur.getErreurs()));
            return true;
        } else {
            System.out.println(afficheur.affichagePotence(this.penduJoueur.getErreurs()));
            System.err.println("Le mot etait " + this.penduJoueur.getMotATrouver() + "\n");
            System.err.println("Vous avez perdu");
            return false;
        }
    }

    /**
     * Permet de connaitre le nom du joueur.
     *
     * @return le nom du joueur.
     */
    public String getIdentifiant() {
        return identifiantJoueur;
    }

    /**
     * Permet de connaitre le pendu du joueur.
     *
     * @return le pendu du joueur.
     */
    public PenduModel getPendu() {
        return penduJoueur;
    }

    /**
     * Permet de connaitre le nombre de mots retournee par le jueur.
     *
     * @return - nombre de mots trouvees.
     */
    public int getNbMotsTrouvees() {
        return nbMotsTrouveesJoueur;
    }

    /**
     * Permet de definir le nombre de mots trouvees.
     *
     * @param nbTrouvees - nb de mot trouvees.
     */
    public void setNbMotsTrouvees(final int nbMotsTrouveesJoueur) {
        this.nbMotsTrouveesJoueur = nbMotsTrouveesJoueur;
    }

}
