package modele;

/**
 * Classe qui permet de construire la liste des mots.
 * @author Rivallin
 *
 */
public class ListeMotsFactory implements AbstractFactory<ListeMots> {

    @Override
    /**
     * Permet de creer la liste en fonction de son type.
     * @return Objet necessaire au fonctionnement du pendu.
     */
    public ListeMots create(final String type, final String param) {
        if ("fichier".equalsIgnoreCase(type)) {
            return new ListeMotsFichier(param);
        } else if ("fixe".equalsIgnoreCase(type)) {
            return new ListeMotsFixe();
        }
        return null;
    }

    @Override
    public ListeMots create(String type, String niveau, String typePendu) {
        return null;
    }

}
