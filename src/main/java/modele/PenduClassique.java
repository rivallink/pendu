package modele;

/**
 * Moteur du Pendu.
 *
 * @author Rivallin
 */
@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
public class PenduClassique extends PenduModel {

    /**
     * Permet de creer le moteur de jeu.
     *
     * @param niveauClassique - le numero du niveau voulu.
     * @param typePenduClassique - le type de Pendu (0 - 1).
     */
    public PenduClassique(final String niveauClassique, final String typePenduClassique) {
        super(niveauClassique, typePenduClassique);
    }

    /**
     * Permet d'essayer une lettre.
     *
     * @param lettre
     *            la lettre a essayer.
     * @return vrai si la lettre est dans le mot.
     */
    public boolean essayerLettre(final char lettre) {
        boolean estDansLeMot = false;
        for (int i = 0; i < super.getMotATrouver().length(); i++) {
            if (Character.toUpperCase(lettre) == super.getMotATrouver().charAt(i)) {
                super.getMotActuel().setCharAt(i, lettre);
                super.setDecouvert(super.getDecouvert() + 1);
                estDansLeMot = true;
            }
        }
        return estDansLeMot;
    }

    /**
     * Permet de modifier le nombre d'esssais globale pour le mode classique.
     *
     * @return le nombre d'essai en mode normale.
     */
    public final int getNombreEssai() {
        return 10;
    }
}
