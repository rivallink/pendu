package modele;

/**
 * Pattern AbstractFactory.
 * @author Rivallin
 *
 * @param <T> Type de l'objet a creer.
 */
public interface AbstractFactory<T> {
    /**
     * Schema des constructeur possibles (par liste fichier).
     * @param type Type de base (fix� ou fichier).
     * @param param Le chemin du fichier.
     * @return Une instance de la classe.
     */
    T create(String type, String param);

    /**
     * Schemae des constructeur possibles (par liste fichier).
     * @param type Type : Tordu ou classique.
     * @param niveau Le niveau choisi par le joueur.
     * @param typePendu Type de base (fix� ou fichier).
     * @return une instance de la classe.
     */
    T create(String type, String niveau, String typePendu);
}
