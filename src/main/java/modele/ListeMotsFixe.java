package modele;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui permet de gerer une liste de mots fixes.
 * @author Rivallin.
 *
 */
public class ListeMotsFixe implements ListeMots {

    /**
     * Liste des mots.
     */
    private List<String> mots;

    /**
     * Constructeur de la classe.
     */
    public ListeMotsFixe() {
        this.mots = new ArrayList<String>();
        this.mots.add("BUT");
        this.mots.add("NUIT");
        this.mots.add("RIZ");
        this.mots.add("BIERE");
        this.mots.add("TENNIS");
        this.mots.add("DEVELOPPER");
    }

    /**
     * Permet de recuperer la liste des mots.
     * @return la liste des mots.
     */
    @Override
    public List<String> getMots() {
        return this.mots;
    }

    /**
     * Permet de modifier la liste des mots.
     * @param mots - la nouvelle liste de mots.
     */
    public void setMots(List<String> mots) {
        this.mots = mots;
    }
}
