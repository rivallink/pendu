package modele;

/**
 * Moteur du Pendu.
 *
 * @author Rivallin
 */
@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
public class PenduTordu extends PenduModel {

    /**
     * Permet de creer le moteur de jeu.
     *
     * @param niveauTordu - le numero du niveau voulu.
     * @param typePenduTordu - le type de Pendu (0 - 1).
     */
    public PenduTordu(final String niveauTordu, final String typePenduTordu) {
        super(niveauTordu, typePenduTordu);
    }

    /**
     * Permet d'essayer une lettre.
     *
     * @param lettre - la lettre a essayer.
     * @return vrai si la lettre est dans le mot.
     */
    public boolean essayerLettre(final char lettre) {
        boolean estDansLeMot = false;
        for (int i = 0; i < super.getMotATrouver().length(); i++) {
            if (Character.toUpperCase(lettre) == super.getMotATrouver().charAt(i)) {
                if (Character.toUpperCase(lettre) != Character.toUpperCase(super.getMotActuel().charAt(i))) {
                    super.setDecouvert(super.getDecouvert() + 1);
                    super.getMotActuel().setCharAt(i, lettre);
                    estDansLeMot = true;
                    break;
                }
            }
        }
        return estDansLeMot;
    }

    /**
     * Permet de modifier le nombre d'esssais globale pour le mode tordu.
     *
     * @return le nombre d'essai en mode tordu.
     */
    public final int getNombreEssai() {
        return 20;
    }
}
