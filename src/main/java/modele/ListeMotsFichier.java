package modele;

import java.io.File;
import java.util.List;

import outils.RwFileOutil;

/**
 * Classe qui permet de gerer une liste de mots par fichier.
 * @author Rivallin
 *
 */
public class ListeMotsFichier implements ListeMots {

    /**
     * Liste des mots.
     */
    private List<String> mots;

    /**
     * Constructeur de la classe.
     * @param param - le chemin du fichier.
     */
    public ListeMotsFichier(final String param) {
        final File file = new File(param);
        if (file.exists()) {
            this.mots = RwFileOutil.readFile(param);
        } else {
            System.err.println("Le fichier n'existe pas");
        }
    }

    /**
     * Permet de recuperer la liste des mots.
     * @return la liste de mots.
     */
    @Override
    public List<String> getMots() {
        return this.mots;
    }

    /**
     * Permet de modifier la liste des mots.
     * @param mots - la nouvelle liste de mots.
     */
    public void setMots(List<String> mots) {
        this.mots = mots;
    }

}
