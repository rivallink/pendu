package modele;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Classe pour gerer le multijoueur.
 *
 * @author Rivallin
 *
 */
@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
public class Multi {
    /**
     * Liste des participants du jeu
     */
    private List<PenduParticipant> listeParticipants;

    /**
     * Construteur de la classe.
     *
     * @param nbIA - nombre d'IA a ajouter au jeu.
     * @param nbJoueur - nombre de joueur.
     * @param moteur - moteru du jeu.
     * @param niveau - niveau du jeu.
     * @param base - base de mots du jeu.
     */
    public Multi(int nbIA, int nbJoueur, String moteur, String niveau, String base) {
        this.listeParticipants = new ArrayList<PenduParticipant>();
        for (int i = 0; i < nbIA; i++) {
            PenduModel model = new PenduFactory().create(moteur, niveau, base);
            listeParticipants.add(new PenduIA("Bot" + Integer.toString(i), model, 0));
        }
        for (int i = 0; i < nbJoueur; i++) {
            PenduModel model = new PenduFactory().create(moteur, niveau, base);
            listeParticipants.add(new PenduJoueur("Joueur " + Integer.toString(i), model, 0));
        }
    }

    /**
     * Gerer le fonctionnement d'une partie multijoueur.
     */
    public void partie() {
        while (!listeParticipants.isEmpty()) {
            for (int i = 0; i < listeParticipants.size(); i++) {
                listeParticipants.get(i).getPendu().init();
                reiniMot();
                System.out.println("Le " + listeParticipants.get(i).getIdentifiant() + " joue");
                if (listeParticipants.get(i).jouer()) {
                    for (int j = 0; j < 50; ++j) {
                        System.out.println();
                    }
                    int nbtrouve = listeParticipants.get(i).getNbMotsTrouvees() + 1;
                    listeParticipants.get(i).setNbMotsTrouvees(nbtrouve);
                    System.out.println("Gagn� pour ce mot!");
                } else {
                    System.out.println("Perdu... le mot etait "
                            + listeParticipants.get(i).getPendu().getMotATrouver());
                    if (listeParticipants.size() > 1) {
                        System.out.println("Passer au joueur suivant");
                         Scanner scan = new Scanner(System.in);
                         scan.nextLine();
                    } else {
                        int nbTrouveTotal = listeParticipants.get(i).getNbMotsTrouvees();
                        System.out.println("Vous avez gagn� avec un score de " + nbTrouveTotal);
                    }
                    for (int j = 0; j < 50; ++j) {
                        System.out.println();
                    }
                    listeParticipants.remove(i);
                }
            }
        }
    }

    /**
     * Permet de initialiser le mot choisit.
     */
    public void reiniMot() {
        String motATrouver = listeParticipants.get(0).getPendu().getMotATrouver();
        for (int i = 0; i < listeParticipants.size(); i++) {
            listeParticipants.get(i).getPendu().setMotATrouver(motATrouver);
            listeParticipants.get(i).getPendu().setMotActuel();
        }
    }

    /**
     * Permet de connaitre la liste des participants de la partie.
     *
     * @return la liste des joueurs de la partie.
     */
    public List<PenduParticipant> getListeParticipants() {
        return listeParticipants;
    }

    /**
     * Permet de chosiir la liste des participants de la partie.
     *
     * @param participants - la liste des joueurs de la partie.
     */
    public void setListeParticipants(final List<PenduParticipant> listeParticipants) {
        this.listeParticipants = listeParticipants;
    }
}
