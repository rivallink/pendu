package modele;

/**
 * Interface pour gerer les types de participants.
 * @author Rivallin
 *
 */
public interface PenduParticipant {
    /**
     * Permet de connaitre le nombre de mots trouvees par le participant.
     * @return nombre de mots trouvees.
     */
    int getNbMotsTrouvees();
    /**
     * Permet de modifier le nombre de mots trouvees.
     * @param nbTrouvees - nombre de motes trouvees.
     */
    void setNbMotsTrouvees(final int nbTrouvees);
    /**
     * Permet de definir le comportement de jeu.
     * @return vrai si il peut jouer, faux sinon.
     */
    boolean jouer();
    /**
     * Permet de recuperer l'identifiant du joueurs.
     * @return l'identifiant du joueur.
     */
    String getIdentifiant();
    /**
     * Permet de recuperer le pendu.
     * @return le pendu.
     */
    PenduModel getPendu();
}
