package modele;

import java.util.List;
import java.util.Random;

/**
 * Model du Pendu.
 *
 * @author Rivallin
 */
@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
public abstract class PenduModel {
    /**
     * Constante pour definir la taille de mot a trouver.
     */
    private static final int SIZESTRINGBUILDER = 20;

    /**
     * Niveau de jeu du pendu : 0 - Debutant, 1 - Expert.
     */
    private String niveau;
    /**
     * Liste des mots du jeu.
     */
    private ListeMots baseMots;
    /**
     * Type du pendu.
     */
    private String typePendu;
    /**
     * Chemin du fichier, si le typePendu est fichier.
     */
    private String chemin;
    /**
     * Nombre d'essai du moteur.
     */
    private int nombreEssai;


    /**
     * Mot aleatoire a trouver.
     */
    private String motATrouver;
    /**
     * Situation actuel du mot du joueur.
     */
    private StringBuilder motActuel = new StringBuilder(SIZESTRINGBUILDER);
    /**
     * Liste des mots du jeu.
     */
    private List<String> mots;
    /**
     * Permet de savoir si le joueur a fini le jeu.
     */
    private boolean victoire;
    /**
     * Nombre de lettres decouvertes.
     */
    private int decouvert;
    /**
     * Nombre d'erreurs.
     */
    private int erreurs;

    /**
     * Permet de creer le moteur de jeu.
     *
     * @param niveau    le numero du niveau voulu.
     * @param typePendu le type de Pendu (0 - 1).
     */
    public PenduModel(final String niveau, final String typePendu) {
        this.niveau = niveau;
        this.typePendu = typePendu;
    }

    /**
     * Permet d'initialiser le moteur de jeu.
     */
    public void init() {
        victoire = false;
        decouvert = 0;
        erreurs = 0;
        this.createList();
        this.choixMot();
    }

    /**
     * Creer la liste des mots en fonction du niveau de jeu choisit.
     */
    public void createList() {
        final ListeMotsFactory fabriqueListeMots = new ListeMotsFactory();
        this.baseMots = fabriqueListeMots.create(this.typePendu, this.chemin);
        this.mots = baseMots.getMots();
    }

    /**
     * Choisit un mot au hasard dans la liste des mots.
     */
    public void choixMot() {
        if (mots != null) {
            final Random aleatoire = new Random();
            /*this.motATrouver = new StringBuilder(mots.get(aleatoire.nextInt(mots.size())));*/
            this.motATrouver = mots.get(aleatoire.nextInt(mots.size()));
            if (this.niveau.equals("0")) {
                while (this.motATrouver.length() < 3 || this.motATrouver.length() > 4) {
                    /*this.motATrouver = new StringBuilder(mots.get(aleatoire.nextInt(mots.size())));*/
                    this.motATrouver = mots.get(aleatoire.nextInt(mots.size()));
                }
            } else if (this.niveau.equals("1")) {
                while (this.motATrouver.length() < 4) {
                    /*this.motATrouver = new StringBuilder(mots.get(aleatoire.nextInt(mots.size())));*/
                    this.motATrouver = mots.get(aleatoire.nextInt(mots.size()));
                }
            }
            this.motActuel = new StringBuilder();
            this.motActuel.setLength(motATrouver.length());
            for (int i = 0; i < motATrouver.length(); i++) {
                this.motActuel.setCharAt(i, '-');
            }
        }
    }

    /**
     * Permet d'essayer une lettre.
     * @param lettre - la lettre a essayer.
     * @return vrai si la lettre est dans le mot.
     */
    public abstract boolean essayerLettre(final char lettre);

    /**
     * Simule un tour de pendu.
     * @param lettre entree par le joueur.
     * @return vrai si le jeu est termine, gagne ou non.
     */
    public boolean tour(final char lettre) {
        boolean finDuJeu = false;
        if (essayerLettre(lettre)) {
            if (decouvert >= motATrouver.length()) {
                finDuJeu = true;
                this.victoire = true;
            }
        } else {
            this.erreurs++;
            if (this.erreurs >= getNombreEssai()) {
                finDuJeu = true;
            }
        }
        return finDuJeu;
    }

    /**
     * Permet de recuperer le niveau du pendu.
     * @return 0 debutant - 1 expert.
     */
    public String getNiveau() {
        return niveau;
    }

    /**
     * Permet de modifier le niveau du pendu.
     * @param niveau Le niveau choisis.
     */
    public void setNiveau(final String niveau) {
        this.niveau = niveau;
    }

    /**
     * Permet de recuperer la base de donnees du jeu.
     * @return base de mots.
     */
    public ListeMots getBaseMots() {
        return baseMots;
    }

    /**
     * Permet de modifier la base de donnes du jeu.
     * @param baseMots La liste de mots.
     */
    public void setBaseMots(final ListeMots baseMots) {
        this.baseMots = baseMots;
    }

    /**
     * Permet de recuperer le type de pendu.
     * @return fichier ou fixe.
     */
    public String getTypePendu() {
        return typePendu;
    }

    /**
     * Permet de modifier le type de pendu.
     * @param typePendu - fichier ou fixe.
     */
    public void setTypePendu(final String typePendu) {
        this.typePendu = typePendu;
    }

    /**
     * Permet de recuperer le chemin du fichier.
     * @return chemin du fichier.
     */
    public String getChemin() {
        return chemin;
    }

    /**
     * Permet de modifier le chemin du fichier.
     * @param chemin Chemin du nouveau fichier.
     */
    public void setChemin(final String chemin) {
        this.chemin = chemin;
    }

    /**
     * Permet de modifier le mot a trouver.
     * @return le mot a trouver.
     */
    public String getMotATrouver() {
        return motATrouver;
    }

    /**
     * Permet de modifier le mot a trouver.
     * @param motATrouver Le nouveau mot a trouver.
     */
    public void setMotATrouver(final String motATrouver) {
        this.motATrouver = motATrouver;
    }


    /**
     * Permet de recuperer le mot actuel du joueur.
     * @return le mot actuel.
     */
    public StringBuilder getMotActuel() {
        return motActuel;
    }

    /**
     * Permet de modifier le mot actuel de joueur.
     */
    public void setMotActuel() {
    	 this.motActuel = new StringBuilder();
         this.motActuel.setLength(motATrouver.length());
         for (int i = 0; i < motATrouver.length(); i++) {
             this.motActuel.setCharAt(i, '-');
         }
    }

    /**.
     * Permet de recuperer la liste de mots.
     * @return liste des mots.
     */
    public List<String> getMots() {
        return mots;
    }

    /**
     * Permet de modifier la liste de mots.
     * @param mots La nouvelle liste de mots.
     */
    public void setMots(final List<String> mots) {
        this.mots = mots;
    }

    /**
     * Permet de savoir si le joueur a gagner.
     * @return vrai si le joueur a gagner.
     */
    public boolean isVictoire() {
        return victoire;
    }

    /**
     * Permet de definir si le joueur a gagner.
     * @param victoire La nouvelle valeur de victoire (true false).
     */
    public void setVictoire(final boolean victoire) {
        this.victoire = victoire;
    }

    /**
     * Permet de connaitre le nombre de lettre decouverte.
     * @return nombre de lettre decouvertes.
     */
    public int getDecouvert() {
        return decouvert;
    }

    /**
     * Permet de modifier le nombre de lettre decouverte.
     * @param decouvert Le nouveau nombre de lettre d�couverte.
     */
    public void setDecouvert(final int decouvert) {
        this.decouvert = decouvert;
    }

    /**
     * Permet de connaitre le nombre d'erreurs commise par le joueur.
     * @return nombre d'erreur.
     */
    public int getErreurs() {
        return erreurs;
    }

    /**
     * Permet de modifier le nombre d'erreur commise par le joueur.
     * @param erreurs Le nouveau nombre d'erreurs.
     */
    public void setErreurs(final int erreurs) {
        this.erreurs = erreurs;
    }

    /**
     * Permet de r�cup�rer le nombre d'essai du joueur.
     * @return le nombre d'essais
     */
    public int getNombreEssai() {
        return 	nombreEssai;
    }

    /**
     * Permet de modifier le nombre d'essais du joueur.
     * @param nombreEssai Le nouveau nombre d'essais.
     */
    public void setNombreEssai(final int nombreEssai) {
        this.nombreEssai = nombreEssai;
    }
}
