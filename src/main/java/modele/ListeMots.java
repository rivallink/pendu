package modele;

import java.util.*;

/**
 * Interface qui permet d'utiliser plusieurs types de liste de mots.
 * @author Rivallin
 *
 */
public interface ListeMots {
    /**
     * Methode qui renvoit les differents mots de la liste.
     * @return la liste de mots.
     */
    List<String> getMots();
}
