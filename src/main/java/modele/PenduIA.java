package modele;

import vue.PenduConsole;

/**
 * Classe qui simule le comportement d'un joueur ordinateur.
 *
 * @author Rivallin.
 *
 */
@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
public class PenduIA implements PenduParticipant {
    /**
     * Tableau des fr�quences.
     */
    private static final char[] FREQUENCE = new char[] {'E', 'A', 'S', 'I', 'N', 'T', 'R', 'L', 'U', 'O', 'D', 'C',
            'P', 'M', 'V', 'G', 'F', 'B', 'Q', 'H', 'X', 'J', 'Y', 'Z', 'K', 'W' };

    /**
     * Identifiant du joueur.
     */
    private final String identifiantIA;

    /**
     * Pendu du joueur.
     */
    private final PenduModel penduIA;

    /**
     * Nombre de mots trouvees par le bot.
     */
    private int nbMotsTrouveesIA;

    /**
     * Constructeur de la classe.
     *
     * @param identifiantIA - identifiant du joueur.
     * @param penduIA - pendu du joueur.
     * @param nbMotsTrouveesIA - nombre de mots trouvees du joueur.
     */
    public PenduIA(final String identifiantIA, final PenduModel penduIA, final int nbMotsTrouveesIA) {
        this.nbMotsTrouveesIA = nbMotsTrouveesIA;
        this.identifiantIA = identifiantIA;
        this.penduIA = penduIA;
    }

    /**
     * Simule le comportement de l'IA.
     * @return vrai si elle peut jouer faux sinon
     */
    public boolean jouer() {
        final PenduConsole afficheur = new PenduConsole();
        for (char caractere : FREQUENCE) {
            if (this.penduIA.tour(caractere)) {
                return this.penduIA.isVictoire();
            } else {
                System.out.println(caractere);
                System.out.println(afficheur.affichagePotence(this.penduIA.getErreurs()));
                System.out.println(this.penduIA.getMotActuel());
                System.out.println("Essais restant :" + (this.penduIA.getNombreEssai() - this.penduIA.getErreurs()));
            }
        }
        return false;
    }

    /**
     * Permet de connaitre l'identifiant du joueur.
     *
     * @return identifiant de l'IA.
     */
    public String getIdentifiant() {
        return identifiantIA;
    }

    /**
     * Permet de connaitre le pendu du joueur.
     *
     * @return pendu du joueur.
     */
    public PenduModel getPendu() {
        return penduIA;
    }

    /**
     * Connaitre le nombre de mots trouvees par le joueur.
     *
     * @return le nombre de mots trouvees par le joueuer.
     */
    public int getNbMotsTrouvees() {
        return nbMotsTrouveesIA;
    }

    /**
     * Permet de definir le nombre de mots trouvees.
     *
     * @param nbTrouvees - nb de mot trouvees
     */
    public void setNbMotsTrouvees(final int nbTrouveesIA) {
        this.nbMotsTrouveesIA = nbTrouveesIA;
    }
}
