package modele;

/**
 * Classe qui permet de construire la liste des mots.
 * @author Rivallin
 *
 */
public class PenduFactory implements AbstractFactory<PenduModel> {

    @Override
    /**
     * Permet de creer la liste en fonction de son type.
     * @return Objet necessaire au fonctionnement du pendu.
     */
    public PenduModel create(final String type, final String niveau, final String typePendu) {
        if ("classique".equalsIgnoreCase(type)) {
            return new PenduClassique(niveau, typePendu);
        } else if ("tordu".equalsIgnoreCase(type)) {
            return new PenduTordu(niveau, typePendu);
        }
        return null;
    }

    @Override
    public PenduModel create(String type, String param) {
        return null;
    }

}
