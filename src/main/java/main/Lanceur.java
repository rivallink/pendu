package main;

import java.io.File;
import java.util.Scanner;

import modele.Multi;
import modele.PenduFactory;
import modele.PenduModel;
import outils.EstChiffreOutil;
import vue.PenduConsole;

/**
 * Classe de Lanceur.
 *
 * @author Rivallin
 *
 */
@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
public class Lanceur {

    /**
     * Moteur du jeu.
     */
    private static PenduModel moteur;

    /**
     * Entree clavier
     */
    private static Scanner clavier = new Scanner(System.in);
    /**
     * Numero du mode de base de liste de mots FIXE
     */
    private static final int FIXE = 0;
    /**
     * Numero du mode de base de liste de mots par FICHIER
     */
    private static final int FICHIER = 1;
    /**
     * Chaine representant le choix d'une base de mot par fichier : FICHIER
     */
    private static final String FICHIERTEXT = "fichier";
    /**
     * Chaine representant le choix du niveau choisi par le joueur
     */
    private static final String ERREURTYPEPENDU = "Votre type de pendu n'existe pas : 0 ou 1 uniquement";

    /**
     * Chaine repreentant le choix du niveau choisi par le joueur
     */
    private static final String ERREURMOTEUR = "Votre type de moteur n'existe pas : 0 ou 1 uniquement";

    /**
     * Chaine representant le choix du niveau choisi par le joueur
     */
    private static final String ERREURNIVEAU = "Votre type de niveau n'existe pas : 0 ou 1 uniquement";

    /**
     * Liste des arguments.
     */
    private String[] args;

    /**
     * Taille des arguments
     */
    private static final int ARGUMENTTAILLE = 2;

    /**
     * Argument 1
     */
    private static final int ARGUMENT1 = 1;

    /**
     * Argument 4
     */
    private static final int ARGUMENT4 = 4;
    /**
     * Chaine representant le choix du niveau choisi par le joueur
     */
    private static final String ERREUR = "Votre niveau n'existe pas : 0 ou 1 uniquement";

    /**
     * Constructeur du lanceur.
     */
    public Lanceur(String[] args) {
        this.args = args;
    }

    /**
     * Lancement du jeu
     *
     * @param args - arguments.
     */
    public void init() {
        if (this.args != null && this.args.length > ARGUMENTTAILLE) {
            analyseArgs();
            if (moteur != null) {
                moteur.init();
                lancementDuJeu();
            }
        } else {
            demandeParametrage();
        }
    }

    /**
     * Analyser les arguments pre-definis lors du lancement.
     */
    public void analyseArgs() {
        final String argMoteur = args[0];
        final String argNiveau = args[1];
        final String argType = args[2];
        String type = null;
        String leMoteur = null;
        if (Integer.valueOf(argMoteur) == 0) {
            leMoteur = "classique";
        } else if (Integer.valueOf(argMoteur) == ARGUMENT1) {
            leMoteur = "tordu";
        }
        if (Integer.valueOf(argType) == 0) {
            type = "fixe";
        } else if (Integer.valueOf(argType) == ARGUMENT1) {
            type = "fichier";
        }
        final PenduFactory argFabriquePendu = new PenduFactory();
        moteur = argFabriquePendu.create(leMoteur, argNiveau, type);
        if (args.length == ARGUMENT4) {
            final String argChemin = args[3];
            moteur.setChemin(argChemin);
        }
    }

    /**
     * Permet de lancer le demarrage du jeu.
     */
    public void demandeParametrage() {
        final int[] multi = choixMulti();
        final String moteurPendu = choixMoteur();
        final String niveau = choixNiveau();
        final String typePendu = choixTypePendu();
        final PenduFactory fabriquePendu = new PenduFactory();
        if (multi[0] == 1) {
            Multi multijoueur = new Multi(multi[1], multi[2], moteurPendu, niveau, typePendu);
            multijoueur.partie();
        } else {
            moteur = fabriquePendu.create(moteurPendu, niveau, typePendu);
            if (typePendu.equals(FICHIERTEXT)) {
                System.out.println("====|  Veuillez choisir le chemin de votre fichier |====");
                final String chemin = choixModeFichier(clavier.nextLine());
                moteur.setChemin(chemin);
            }

            if (moteur != null) {
                moteur.init();
                lancementDuJeu();
            }
        }
    }

    /**
     * Demander � l'utilsateur le nombre de joueur et d'ia
     *
     * @return un tableau de nombre de joueur par type (joueur, ia)
     */
    private static int[] choixMulti() {
        System.out.println("====|  Voulez vous jouer en multi ? - 0 pour non ou 1 pour oui |====");
        int[] multiParam = new int[3];
        while (true) {
            final String modeChoisit = clavier.nextLine();
            if (EstChiffreOutil.main(modeChoisit)) {
                int mode = Integer.parseInt(modeChoisit);
                if (mode < 2 && mode > -1) {
                    if (mode == FIXE) {
                        multiParam[0] = FIXE;
                        break;
                    } else if (mode == FICHIER) {
                        multiParam[0] = FICHIER;
                        System.out.println("====|  Indiquer le nombre d'IA que vous voulez ? |====");
                        multiParam[1] = Integer.parseInt(clavier.nextLine());
                        System.out.println("====|  Indiquer le nombre de joueurs que vous voulez ? |====");
                        multiParam[2] = Integer.parseInt(clavier.nextLine());
                    }
                    break;
                } else {
                    System.err.println(ERREUR);
                }
            } else {
                System.err.println(ERREUR);
            }
        }
        return multiParam;
    }

    /**
     * Permet de choisir la base de mots utilise.
     *
     * @return fixe si 0 et fichier si 1.
     */
    public static String choixTypePendu() {
        System.out
                .println("====|  Veuillez choisir votre mode - 0 pour Base de mots fixe ou 1 pour votre fichier |====");
        String typePendu = null;
        while (true) {
            final String modeTypePendu = clavier.nextLine();
            if (EstChiffreOutil.main(modeTypePendu)) {
                int mode = Integer.parseInt(modeTypePendu);
                if (mode < 2 && mode > -1) {
                    if (mode == FIXE) {
                        typePendu = "fixe";
                    } else if (mode == FICHIER) {
                        typePendu = FICHIERTEXT;
                    }
                    break;
                } else {
                    System.err.println(ERREURTYPEPENDU);
                }
            } else {
                System.err.println(ERREURTYPEPENDU);
            }
        }
        return typePendu;
    }

    /**
     * Permet de choisir le moteur du pendu.
     *
     * @return classique si xe si 0 et tordu si 1.
     */
    public static String choixMoteur() {
        System.out.println("====|  Veuillez choisir votre moteur - 0 pour le classique ou 1 le tordu |====");
        String moteurChoisit = null;
        while (true) {
            final String modeMoteur = clavier.nextLine();
            if (EstChiffreOutil.main(modeMoteur)) {
                int mode = Integer.parseInt(modeMoteur);
                if (mode < 2 && mode > -1) {
                    if (mode == FIXE) {
                        moteurChoisit = "classique";
                    } else if (mode == FICHIER) {
                        moteurChoisit = "tordu";
                    }
                    break;
                } else {
                    System.err.println(ERREURMOTEUR);
                }
            } else {
                System.err.println(ERREURMOTEUR);
            }
        }
        return moteurChoisit;
    }

    /**
     * Permet de choisir le niveau du pendu.
     *
     * @return 0 debutant - 1 expert.
     */
    public static String choixNiveau() {
        System.out.println("====|  Veuillez choisir votre niveau - 0 pour Debutant ou 1 pour Expert |====");
        int modeNiveau = 0;
        while (true) {
            final String modeChoisitNiveau = clavier.nextLine();
            if (EstChiffreOutil.main(modeChoisitNiveau)) {
                modeNiveau = Integer.parseInt(modeChoisitNiveau);
                if (modeNiveau < 2 && modeNiveau > -1) {
                    break;
                } else {
                    System.err.println(ERREURNIVEAU);
                }
            } else {
                System.err.println(ERREURNIVEAU);
            }
        }
        return String.valueOf(modeNiveau);
    }

    /**
     * Permet de specifier le chemin du fichier.
     *
     * @param chemin Le chemin du fichier
     * @return string - chemin du fichier.
     */
    public static String choixModeFichier(String chemin) {
        File fichier = new File(chemin);
        String bonChemin = chemin;
        if (!fichier.exists()) {
            System.err.println("Le fichier n'existe pas");
            System.out.println("====|  Veuillez choisir le chemin de votre fichier |====");
            bonChemin = choixModeFichier(clavier.nextLine());
        }
        return bonChemin;
    }

    /**
     * Permet de gerer le fonctionnement du pendu.
     */
    public static void lancementDuJeu() {
        System.out.println();
        System.out.println(moteur.getMotActuel());
        System.out.println("Nombre d'essais :" + (moteur.getNombreEssai() - moteur.getErreurs()));
        String scanlancement = clavier.nextLine();
        final PenduConsole afficheur = new PenduConsole();
        while (scanlancement.length() < 1) {
            scanlancement = clavier.nextLine();
        }
        while (!moteur.tour(scanlancement.charAt(0))) {
            System.out.println(afficheur.affichagePotence(moteur.getErreurs()));
            System.out.println(moteur.getMotActuel());
            System.out.println("Essais restant :" + (moteur.getNombreEssai() - moteur.getErreurs()));
            scanlancement = clavier.nextLine();
            while (scanlancement.length() < 1) {
                scanlancement = clavier.nextLine();
            }
        }
        if (moteur.isVictoire()) {
            System.out.println("GAGNE ! \n Vous avez trouve le mot :" + moteur.getMotATrouver() + "\n essais restant :"
                    + (moteur.getNombreEssai() - moteur.getErreurs()));
        } else {
            System.out.println(afficheur.affichagePotence(moteur.getErreurs()));
            System.err.println("Le mot etait " + moteur.getMotATrouver() + "\n");
            System.err.println("Vous avez perdu");
        }
    }

    /**
     * Recuperer le moteur.
     *
     * @return le moteur
     */
    public PenduModel getMoteur() {
        return moteur;
    }

    /**
     * Recuperer les arguments.
     *
     * @return les arguments
     */
    public String[] getArgs() {
        return args;
    }

}
