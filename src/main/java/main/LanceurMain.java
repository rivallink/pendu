package main;

import java.util.Scanner;

import outils.EstChiffreOutil;

/**
 * Permet de lancer le Pendu.
 *
 * @author Rivallin
 *
 */
@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
public final class LanceurMain {

    /**
     * Entree clavier
     */
    private static Scanner clavier = new Scanner(System.in);
    /**
     * Chaine repr�sentant le choix du niveau choisi par le joueur
     */
    private static final String ERREURREJOUER = "Votre type pour rejouer n'existe pas : 0 ou 1 uniquement";


    /**
     * Constructeur vide du lanceur
     */
    private LanceurMain() {
    }

    /**
     * Lance le jeu du Pendu.
     *
     * @param args
     *            Liste des arguments
     */
    public static void main(String[] args) {
        System.out.println("====|  Lancement du Pendu |====");
        Lanceur lanceur = new Lanceur(args);
        lanceur.init();

        final boolean rejouer = rejouer();
        if (rejouer) {
            main(null);
        } else {
            clavier.close();
        }
    }

    /**
     * Permet de relancer le moteur de jeu et les differents choix.
     *
     * @return Le joueur souhaite-t-il rejouer : vrai oui - false non
     */
    public static boolean rejouer() {
        System.out.println("====|  Voulez vous rejouer - 0 pour non ou 1 pour oui |====");
        int rejouer = 0;
        while (true) {
            final String modeChoisitRejouer = clavier.nextLine();
            if (EstChiffreOutil.main(modeChoisitRejouer)) {
                rejouer = Integer.parseInt(modeChoisitRejouer);
                if (rejouer < 2 && rejouer > -1) {
                    break;
                } else {
                    System.err.println(ERREURREJOUER);
                }
            } else {
                System.err.println(ERREURREJOUER);
            }
        }
        boolean ret = false;
        if (rejouer > 0) {
            ret = true;
        }
        return ret;
    }
}
