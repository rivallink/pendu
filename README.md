# Convention de nommages

## GIT

### Nommage d'une branche : type/intitule
	Le type correspond à :
		- fea si c'est la mise en place d'une nouvelle fonctionnalité
		- fix si c'est une correction d'un bug
	L'intitule est un cours descriptif du but de la branche
	Les deux champs séparés d'un '/' (slash) sans espace

### Message de commit : PN - Message du commit
	Initiale du prénom et du nom
	Le message commence par une majuscule
	Les deux champs séparés d'un '-' (tiret) avec un espace de chaque côté

## JAVA

### Langue global du projet et du code
	Francais-fr-FR

### Declaration des interfaces : MonInterface
	Premiere lettre en majuscule
	Chaque premiere de mot en majuscule


### Declaration des classes : MaClasse
	Premiere lettre en majuscule
	Chaque premiere de mot en majuscule

### Declaration des methodes : maMethode
	Premiere lettre en minuscule
	Chaque premiere de mot en majuscule

### Declaration des variables : maVariable
	Premiere lettre en minuscule
	Chaque premiere de mot en majuscule

### Declaration des contantes: MA_CONSTANTE
	Toutes les lettres en majuscule
	Chaque mot separe d'un underscore

### Commentaires
	Utilisation de la javadoc

